


import Classes.Ponto;

public class Programa {
    public static void main(String[] args) {
        
        Ponto ponto1 = new Ponto();

        
        Ponto ponto2 = new Ponto(2, 5);

        
        double distanciaPonto1Ponto2 = ponto1.calcularDistancia(ponto2);
        System.out.println("Distância do ponto1 ao ponto2: " + distanciaPonto1Ponto2);

        
        Ponto coordenadas7_2 = new Ponto(7, 2);
        double distanciaPonto2Coordenadas7_2 = ponto2.calcularDistancia(coordenadas7_2);
        System.out.println("Distância do ponto2 às coordenadas (7,2): " + distanciaPonto2Coordenadas7_2);

        
        ponto1.setX(10);

        
        ponto1.setY(3);
    }
}
