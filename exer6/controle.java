class AeroportoController
{
    private List<Passageiro> passageiros = new List<Passageiro>();
    private List<Comandante> comandantes = new List<Comandante>();
    private List<Comissario> comissarios = new List<Comissario>();

    public void AdicionarPassageiro(Passageiro passageiro)
    {
        passageiros.Add(passageiro);
    }

    public void AdicionarComandante(Comandante comandante)
    {
        comandantes.Add(comandante);
    }

    public void AdicionarComissario(Comissario comissario)
    {
        comissarios.Add(comissario);
    }

    // Outras operações de CRUD para passageiros, comandantes, comissários e aeronaves
}

