class Program
{
    static void Main(string[] args)
    {
        AeroportoController controller = new AeroportoController();

        while (true)
        {
            Console.WriteLine("1. Adicionar Passageiro");
            Console.WriteLine("2. Adicionar Comandante");
            Console.WriteLine("3. Adicionar Comissário");
            Console.WriteLine("4. Sair");
            Console.WriteLine("Escolha uma opção:");

            int opcao = Convert.ToInt32(Console.ReadLine());

            switch (opcao)
            {
                case 1:
                    Passageiro passageiro = AeroportoView.CriarPassageiro();
                    controller.AdicionarPassageiro(passageiro);
                    break;
                case 2:
                    // Lógica para adicionar comandante
                    break;
                case 3:
                    // Lógica para adicionar comissário
                    break;
                case 4:
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Opção inválida");
                    break;
            }
        }
    }
}