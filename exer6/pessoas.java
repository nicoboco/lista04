using System;
using System.Collections.Generic;

// Model
class Pessoa
{
    public string Nome { get; set; }
    public string RG { get; set; }
}

class Passageiro : Pessoa
{
    public string IdentificadorBagagem { get; set; }
    public Passagem Passagem { get; set; }
}

class Tripulacao : Pessoa
{
    public string IdentificacaoAeronautica { get; set; }
    public string MatriculaFuncionario { get; set; }
}

class Comandante : Tripulacao
{
    public int TotalHorasVoo { get; set; }
}

class Comissario : Tripulacao
{
    public List<string> IdiomasFluencia { get; set; }
}

class Aeronave
{
    public string Codigo { get; set; }
    public string Tipo { get; set; }
    public int QuantidadeAssentos { get; set; }
}

class Passagem
{
    public int NumeroAcento { get; set; }
    public string ClasseAcento { get; set; }
    public DateTime DataVoo { get; set; }
}
