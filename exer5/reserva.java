class Reserva
{
    public string Responsavel { get; set; }
    public int QuantidadePessoas { get; set; }
    public DateTime DataReserva { get; set; }
    public decimal ValorTotal { get; set; }
}
