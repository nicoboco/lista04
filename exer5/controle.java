class EventoController
{
    private List<Evento> eventos = new List<Evento>();
    private List<Reserva> reservas = new List<Reserva>();

    public void AdicionarEvento(Evento evento)
    {
        eventos.Add(evento);
    }

    public void ListarEventos()
    {
        foreach (var evento in eventos)
        {
            Console.WriteLine($"Nome: {evento.Nome}, Data: {evento.Data}, Local: {evento.Local}, Lotação Máxima: {evento.LotacaoMaxima}, Ingressos Vendidos: {evento.IngressosVendidos}, Preço do Ingresso: {evento.PrecoIngresso}");
        }
    }

    // Outras operações de CRUD para eventos

    public void FazerReserva(Evento evento, Reserva reserva)
    {
        evento.IngressosVendidos += reserva.QuantidadePessoas;
        reserva.ValorTotal = reserva.QuantidadePessoas * evento.PrecoIngresso;
        reserva.DataReserva = DateTime.Now;
        reservas.Add(reserva);
    }

    // Outras operações relacionadas a reservas
}
