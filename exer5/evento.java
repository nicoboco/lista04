using System;
using System.Collections.Generic;

// Model
class Evento
{
    public string Nome { get; set; }
    public DateTime Data { get; set; }
    public string Local { get; set; }
    public int LotacaoMaxima { get; set; }
    public int IngressosVendidos { get; set; }
    public decimal PrecoIngresso { get; set; }
}
}
