class Program
{
    static void Main(string[] args)
    {
        EventoController controller = new EventoController();

        while (true)
        {
            Console.WriteLine("1. Adicionar Evento");
            Console.WriteLine("2. Listar Eventos");
            Console.WriteLine("3. Fazer Reserva");
            Console.WriteLine("4. Sair");
            Console.WriteLine("Escolha uma opção:");

            int opcao = Convert.ToInt32(Console.ReadLine());

            switch (opcao)
            {
                case 1:
                    Evento evento = EventoView.CriarEvento();
                    controller.AdicionarEvento(evento);
                    break;
                case 2:
                    controller.ListarEventos();
                    break;
                case 3:
                    // Lógica para fazer reserva
                    break;
                case 4:
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Opção inválida");
                    break;
            }
        }
    }
}